//
//  ViewDelegate.swift
//  exo_ios
//
//  Created by Louis ADAM on 14/02/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

protocol SignInViewDelegate {
    func goToRegister()
    func login(mail: String, pwd: String)
}

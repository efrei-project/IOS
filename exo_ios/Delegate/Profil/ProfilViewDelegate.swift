//
//  ProfilViewDelegate.swift
//  exo_ios
//
//  Created by Louis ADAM on 14/02/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

protocol ProfilViewDelegate {
    func logout()
    func changePassword(pwd1: String, pwd2: String)
}

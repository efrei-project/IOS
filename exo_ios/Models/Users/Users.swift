//
//  Users.swift
//  exo_ios
//
//  Created by Louis ADAM on 14/02/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import Foundation
class User {
    var email: String
    var password: String
    
    init(email: String, password: String){
        self.email = email
        self.password = password
    }    
}

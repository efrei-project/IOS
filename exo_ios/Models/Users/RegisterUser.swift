//
//  RegisterUser.swift
//  exo_ios
//
//  Created by Louis ADAM on 14/02/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import Foundation
class RegisterUser {
    var user: User?
    static let instance: RegisterUser = RegisterUser()
    
    func setUser(mail: String, pwd: String){
        self.user = User(email: mail, password: pwd)
    }
    
    func unSetUser(){
        self.user = nil
    }
}

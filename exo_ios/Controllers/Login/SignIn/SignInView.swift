//
//  SignInView.swift
//  exo_ios
//
//  Created by Louis ADAM on 14/02/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import UIKit

class SignInView: UIView {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var loginError: UILabel!
    
    var delegate: SignInViewDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed("SignInView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.resetField()
    }
    @IBAction func login(_ sender: Any){
        guard let mail = self.emailField.text, let pwd = self.passwordField.text else {
            return
        }
        if mail == "" || pwd == "" {
            self.resetField()
            return
        }
        delegate?.login(mail: mail, pwd: pwd)
    }
    
    @IBAction func goToRegister(_ sender: Any) {
        delegate?.goToRegister()
    }
    
    func setLoginError(arg: Bool){
        self.loginError.isHidden = arg
    }
    
    func resetField(){
        self.emailField.text = ""
        self.passwordField.text = ""
        self.setLoginError(arg: true)
    }
}

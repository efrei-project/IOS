//
//  LoginViewController.swift
//  exo_ios
//
//  Created by Louis ADAM on 14/02/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, SignInViewDelegate, SignUpViewDelegate, ProfilViewDelegate {
    
   
    @IBOutlet weak var signin: SignInView!
    @IBOutlet weak var signup: SignUpView!
    @IBOutlet weak var profilView: ProfilView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.signup.delegate = self
        self.signin.delegate = self
        self.profilView.delegate = self
        self.goToRegister()
    }
    
    func goToLogin() {
        self.resetFields()
        self.signin.isHidden = false
        self.signup.isHidden = true
        self.profilView.isHidden = true
    }
        
    func goToRegister() {
        self.resetFields()
        self.signin.isHidden = true
        self.signup.isHidden = false
        self.profilView.isHidden = true
    }
    
    func goToProfil(){
        self.resetFields()
        self.signin.isHidden = true
        self.signup.isHidden = true
        self.profilView.isHidden = false
    }
    
    func resetFields(){
        self.signup.resetField()
        self.signin.resetField()
        self.profilView.resetField()
    }
    
    func login(mail: String, pwd: String) {
        let user: User = RegisterUser.instance.user ?? User(email: "", password: "")
        if user.email != mail || user.password != pwd {
            self.signin.setLoginError(arg: false)
            print("Please register first")
            return
        }
        self.profilView.setField(mail: user.email)
        self.goToProfil()
        print("Successful login")
        return
    }
    
    func register(mail: String, pwd1: String, pwd2: String){
        if pwd1 != pwd2{
            self.signup.setPwdError(arg: false)
            return
        }
        RegisterUser.instance.setUser(mail: mail, pwd: pwd1)
        self.goToLogin()
        print("Successful sign up")
        return
    }
    
    func logout() {
        self.goToLogin()
    }
    
    func changePassword(pwd1: String, pwd2: String) {
        if pwd1 != pwd2{
            self.profilView.setPwdError(arg: false)
            print("Password not watching")
            return
        }
        RegisterUser.instance.setUser(mail: RegisterUser.instance.user!.email, pwd: pwd1)
        self.goToProfil()
        print("Password change is a success")
        return
    }
    
    func toString(user: User, funcName: String){
        print("--------------------------\n La méthode \(funcName) a été appelée.\n Notre user actuel est :\n\t mail: \(user.email)\n\tpassword: \(user.password)")
    }
    
}

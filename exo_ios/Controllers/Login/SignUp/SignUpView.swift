//
//  SignUpView.swift
//  exo_ios
//
//  Created by Louis ADAM on 14/02/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import UIKit

class SignUpView: UIView {

    @IBOutlet var passOneField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passTwoField: UITextField!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var pwdError: UILabel!
    
    var delegate: SignUpViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("SignUpView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.resetField()
    }
    
    @IBAction func register(_ sender: Any){
        guard let mail = emailField.text, let pass1 = passOneField.text, let pass2 = passTwoField.text else {
            return
        }
        if mail == "" || pass1 == "" || pass2 == ""{
            self.resetField()
            return
        }
        delegate?.register(mail: mail, pwd1: pass1, pwd2: pass2)
    }
    
    @IBAction func goToLogin(_ sender: Any){
        delegate?.goToLogin()
    }
    
    func setPwdError(arg: Bool){
        self.pwdError.isHidden = arg
    }
    
    func resetField(){
        self.passOneField.text = ""
        self.passTwoField.text = ""
        self.emailField.text = ""
        self.setPwdError(arg: true)
    }
}

//
//  ProfilView.swift
//  exo_ios
//
//  Created by Louis ADAM on 14/02/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import UIKit

class ProfilView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var logOutButton: UIButton!
    @IBOutlet weak var passOneField: UITextField!
    @IBOutlet weak var passTwoField: UITextField!
    @IBOutlet weak var pwdError: UILabel!
    var delegate: LoginViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("ProfilView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.resetField()
    }

    func resetField(){
        self.passOneField.text = ""
        self.passTwoField.text = ""
        self.setPwdError(arg: true)
    }
    
    func setField(mail: String){
        self.emailLabel.text = mail
    }
    
    
    func setPwdError(arg: Bool){
        self.pwdError.isHidden = arg
    }
    
    @IBAction func changePassword(_ sender: Any) {
        let tmp = self.passOneField.text!
        delegate?.changePassword(pwd1: tmp, pwd2: self.passTwoField.text!)
    }
    
    @IBAction func logout(_ sender: Any) {
        self.emailLabel.text = ""
        delegate?.logout()
    }
}
